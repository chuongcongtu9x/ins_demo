import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { DatabaseModule } from './database/database.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ChatModule } from './chat/chat.module';
import { SocketModule } from '@nestjs/websockets/socket-module';

@Module({
  imports: [DatabaseModule, UsersModule, AuthModule, ChatModule, SocketModule],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
