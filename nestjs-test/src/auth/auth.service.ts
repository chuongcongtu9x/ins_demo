import {Injectable, UnauthorizedException} from '@nestjs/common';
import {LoginDto} from "./dto/login.dto";
import { JwtService } from '@nestjs/jwt';
import {UsersService} from "../users/users.service";

@Injectable()
export class AuthService {
    constructor(private readonly jwtService: JwtService, private readonly usersService: UsersService) {}

    async login(loginDto: LoginDto): Promise<string> {
        // Thực hiện logic xác thực người dùng
        const user = await this.validateUser(loginDto.username, loginDto.password);

        // Kiểm tra người dùng và tạo mã thông báo (token)
        if (user) {
            const payload = { username: user.username }; // Thông tin mà bạn muốn lưu trữ trong token
            console.log('payload', payload);
            return 'this.jwtService.sign(payload)';
        } else {
            // Xử lý khi xác thực không thành công
            throw new UnauthorizedException('Invalid credentials');
        }
    }

    private async validateUser(username: string, password: string): Promise<any | null> {
        const user = await this.usersService.findByUserName(username);
        console.log(user);
        if (user && user.password === password) {
            return user;
        }

        return null;
    }

}
