import { Module } from '@nestjs/common';
import {AuthService} from "./auth.service";
import {AuthController} from "./auth.controller";
import {JwtModule, JwtService} from "@nestjs/jwt";
import {MongooseModule} from "@nestjs/mongoose";
import {Users, UsersSchema} from "../users/users.schema";
import {UsersService} from "../users/users.service";
import {UsersModule} from "../users/users.module";

@Module({
    imports: [JwtModule.register({
        secret: '12121', // Đặt giá trị cho secretOrPrivateKey
        signOptions: { expiresIn: '1h' }, // Tuỳ chỉnh thời gian hết hạn của JWT
    }),
        MongooseModule.forFeature([{ name: Users.name, schema: UsersSchema }]),
        UsersModule
    ],
    providers: [AuthService, JwtService, UsersService],
    controllers: [AuthController],
})
export class AuthModule {}
