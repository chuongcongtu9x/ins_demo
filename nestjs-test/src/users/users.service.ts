import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import {Users, UsersDocument} from "./users.schema";

@Injectable()
export class UsersService {
    constructor(
        @InjectModel(Users.name) private readonly user: Model<UsersDocument>,
    ) {}

    async findAll(): Promise<Users[]> {
        return await this.user.find().exec();
    }

    async findOne(id: string): Promise<Users> {
        return await this.user.findById(id).exec();
    }

    async create(createTodoDto: any): Promise<Users> {
        return await new this.user({
            ...createTodoDto,
            createdAt: new Date(),
        }).save();
    }

    async update(id: string, updateTodoDto: any): Promise<Users> {
        return await this.user.findByIdAndUpdate(id, updateTodoDto).exec();
    }

    async delete(id: string): Promise<Users> {
        return await this.user.findByIdAndDelete(id).exec();
    }

    async updateContentByCode(code: string, content: string): Promise<Users> {
        return this.user
            .findOneAndUpdate({ code }, { content }, { new: true })
            .exec();
    }

    async findByUserName(username: string): Promise<any> {
        return this.user.findOne({ username }).exec();
    }
}
