import {Component, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {UserService} from "../../services";
import {combineLatest, concat, filter, forkJoin, map, merge, Observable, of, Subject, take, takeUntil} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy, OnChanges {
  uns$ = new Subject();
  isLoginFb = true;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  loginFbChange() {
    this.isLoginFb = !this.isLoginFb;
  }

  ngOnDestroy() {
    this.uns$.next(null);
    this.uns$.complete();
  }
}
