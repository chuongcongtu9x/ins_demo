import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-two',
  templateUrl: './two.component.html',
  styles: [`
    .example-two {
      font-size: 19px;
      margin-bottom: 10px;
    }
  `],
})
export class TwoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input()
  user: any | undefined;

  update() {
    // @ts-ignore
    this.user.name = 'Scott Raynor';
  }

}
