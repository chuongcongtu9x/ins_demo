import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-content-remember',
  templateUrl: './content-remember.component.html',
  styleUrls: ['./content-remember.component.scss']
})
export class ContentRememberComponent implements OnInit {
  @Output() checked: EventEmitter<boolean> = new EventEmitter<boolean>();


  constructor() { }

  ngOnInit(): void {
  }

  onChecked(value: any) {
    const target = value as HTMLInputElement;
    const checked: boolean = target.checked;
    this.checked.emit(checked);
  }
}
