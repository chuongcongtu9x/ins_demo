import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularAdvRoutingModule } from './angular-adv-routing.module';
import { ContentInjectionComponent } from './content-injection/content-injection.component';
import { ContentChildComponent } from './content-child/content-child.component';
import { ContentRememberComponent } from './content-remember/content-remember.component';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';
import { ThreeComponent } from './three/three.component';


@NgModule({
  declarations: [
    ContentInjectionComponent,
    ContentChildComponent,
    ContentRememberComponent,
    OneComponent,
    TwoComponent,
    ThreeComponent,
  ],
  imports: [
    CommonModule,
    AngularAdvRoutingModule
  ]
})
export class AngularAdvModule { }
