import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "../../pages/home/home.component";
import {ContentInjectionComponent} from "./content-injection/content-injection.component";

const routes: Routes = [
  {
    path: '',
    component: ContentInjectionComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AngularAdvRoutingModule { }
