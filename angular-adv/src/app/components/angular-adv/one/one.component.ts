import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styles: [`
    .example-one {
      font-size: 19px;
      margin-bottom: 10px;
    }
  `],
})
export class OneComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() user: any | undefined;

  update() {
    // @ts-ignore
    this.user.name = 'Matt Skiba';
  }

}
