import {AfterContentInit, Component, ContentChild, EventEmitter, OnInit, Output} from '@angular/core';
import {ContentRememberComponent} from "../content-remember/content-remember.component";

@Component({
  selector: 'app-content-child',
  templateUrl: './content-child.component.html',
  styleUrls: ['./content-child.component.scss']
})
export class ContentChildComponent implements AfterContentInit {
  showMessage: boolean | undefined;
  title: string | undefined;

  @ContentChild(ContentRememberComponent) remember: ContentRememberComponent | undefined;

  @Output() submitted: EventEmitter<any> = new EventEmitter<any>();

  ngAfterContentInit() {
    if (this.remember) {
      this.remember.checked.subscribe((checked: boolean) => this.showMessage = checked);
    }
  }

  onSubmit(value: any) {
    this.submitted.emit(value);
  }
}
