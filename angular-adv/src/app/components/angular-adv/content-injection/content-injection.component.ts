import {
  AfterContentInit,
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {ContentChildComponent} from "../content-child/content-child.component";

@Component({
  selector: 'app-content-injection',
  templateUrl: './content-injection.component.html',
  styleUrls: ['./content-injection.component.scss']
})
export class ContentInjectionComponent implements AfterContentInit {
  rememberMe: boolean = false;
  @ViewChild('entry', { read: ViewContainerRef }) entry: ViewContainerRef | undefined;

  constructor(
    private resolver: ComponentFactoryResolver
  ) { }

  ngAfterContentInit() {
    setTimeout(() => {
      const authFormFactory = this.resolver.resolveComponentFactory(ContentChildComponent);
      const component = this.entry?.createComponent(authFormFactory);
      // @ts-ignore
      component.instance.title = 'Hello';
    })
  }

  rememberUser(remember: boolean) {
    this.rememberMe = remember;
  }

  user: any = {
    name: 'Mark Hoppus',
    age: 44,
    location: 'California'
  };

  addProp() {
    this.user.email = 'blink@blink-182.net';
  }

  changeName() {
    this.user.name = 'Travis Barker';
  }

  changeUser() {
    this.user = {
      name: 'Tom Delonge',
      age: 41,
      location: 'California'
    };
  }
}
