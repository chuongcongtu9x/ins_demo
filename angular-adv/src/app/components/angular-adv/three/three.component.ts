import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-three',
  templateUrl: './three.component.html',
  styles: [`
    .example-one {
      border: 2px solid green;
    }
  `],
})
export class ThreeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
